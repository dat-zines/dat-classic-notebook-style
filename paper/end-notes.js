const html = require('choo/html')
const md = require('markdown-it')()
const raw = require('nanohtml/raw')

module.exports = view

function view ({text, endNotes, coverImage}, emit) {
  if (endNotes && text) {
    var coverImage = `../aesthetic/cover-image/${coverImage}`
    return html`
    <body>
    <div id='end-note-container'>
      <section id='cover-stamp'>
      <img id='illustration' 
        src=${coverImage} 
        onclick=${backHome}
        alt='cover image' />
      <h1 id='title'>${text.title}</h1>
      </section>
    <section id='end-notes'>
    <h1>${endNotes.title}</h1>
    ${renderNotes(endNotes)}
    </section>
    <section id='invite-to-copy'>
      <p>Wanna Make your own zine?  You can copy this one to use its structure, then  adjust it to match yr heart!</p>
      <button onclick=${makeCopy}>Make yr Own Copy!</button>
      </section>
    </div>
    </body>
      `
  } else {
    return html`<body><p>loading</p></body>`
  }

  function renderNotes (endNotes) {
    console.log(endNotes.text)
    var notesAsHtml = raw(md.render(endNotes.text))
    return html`${notesAsHtml}`
  }

  function backHome () {
    emit('pushState', '/')
  }
}

async function makeCopy () {
  var self = new DatArchive(window.location)
  var branch = await DatArchive.fork(self, {title: 'name of your zine', description: 'make sure to read the README.  It helps!'})
  window.open(branch.url)
}
