// Bring in our outdoor modules
const choo = require('choo')

// Bring in our indoor modules
const cover = require('./paper/cover')
const text = require('./paper/text')
const endNotes = require('./paper/end-notes.js')

// Initialize choo
const app = choo()

app.use(require('choo-devtools')())

app.use(require('./binding/assembleZine'))
app.use(require('./binding/textNavigation'))

app.mount('body')

app.route('/', cover)
app.route('#text', text)
app.route('#end-notes', endNotes)
