var smarkt = require('smarkt')
var _ = require('lodash')

var archive = new DatArchive(window.location.host)

function store (state, emitter) {
  state.text = {}
  emitter.on('DOMContentLoaded', function () {
    assignTexts()
    assignCoverImage()
    assignZineInfo()
  })

  function assignCoverImage () {
    archive.readdir('aesthetic/cover-image')
      .then(images => {
	for (var image of images) {
          state.coverImage = image
        }
      })
  }

  function assignTexts () {
    archive.readdir('text')
      .then(files => {
        var textFiles = files.filter(file => fileExtension(file) === 'txt')
        mapToState(textFiles)
      })
  }

  function assignZineInfo () {
    archive.readFile('distro/info.txt')
      .then(info => {
        document.querySelector('title').textContent = info.title
        state.info = smarkt.parse(info)
        emitter.emit('pushState')
      })
  }

  function mapToState (textFiles) {
    textFiles.map(text => {
      if (text === 'end-notes.txt') {
        archive.readFile(`text/${text}`).then(EndNoteContents => addEndNotesToState(EndNoteContents))
      } else {
        archive.readFile(`text/${text}`).then(textContents => addTextToState(textContents))
      }
    })
  }

  function addTextToState (text) {
    state.text = smarkt.parse(text)
    state.text.paragraphs = cleanedText(_.split(state.text.text, /^~~\**$/mg))
    emitter.emit('pushState')
    console.log(state.text)
  }

  function addEndNotesToState (notes) {
    state.endNotes = smarkt.parse(notes)
    emitter.emit('render')
  }
}

function cleanedText (arr) {
  return arr.map(text => _.trim(text))
}

function fileExtension (file) {
  return _.last(file.split('.'))
}

module.exports = store
